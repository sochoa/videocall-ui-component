import { tap, switchMap, map, mergeMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

import * as OT from '@opentok/client';
import config from '../config';

@Injectable({
  providedIn: 'root'
})
export class OpentokService {

  session: OT.Session;
  publisher: OT.Publisher;
  token: string;

  constructor(private cliente: HttpClient) { }

  getOT() {
    return OT;
  }

  getToken(idCreacion: number): Observable<string> {
    return this.cliente.get(`http://localhost:8989/api/rest/v1/opentok/session/${idCreacion}`).pipe(
      map((data: any) => data.sessionId)
    )
  }
  initSession(sessionId: string) {
    return this.cliente.get(`http://localhost:8989/api/rest/v1/opentok/join/${sessionId}`).pipe(
      map((data: any) => {
        console.log(data, 'Nuestros datos')
        this.session = this.getOT().initSession(data.apiKey, data.sessionId);
        this.token = data.token;
        return this.session;
      })
    ).toPromise();
    // return fetch(url)
    //   .then((data) => data.json())
    //   .then((json) => {
    //     this.session = this.getOT().initSession(json.apiKey, json.sessionId);
    //     this.token = json.token;
    //     parametro =  this.token;
    //     return this.session;
    //   });
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.session.connect(this.token, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(this.session);
        }
      });
    });
  }

  StopVideo() {
    this.publisher.publishVideo(false);
  }

  startVideo() {
    this.publisher.publishVideo(true);
  }

  muteAudio() {
    this.publisher.publishAudio(false);
  }

  unMuteAudio() {
    this.publisher.publishAudio(true);
  }

  leaveConversation() {
    this.session.disconnect();
    setTimeout(function afterTwoSeconds() {
      window.close();
    }, 4000);
  }

}





