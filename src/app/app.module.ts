import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector  } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PublisherComponent } from './publisher/publisher.component';
import { SubscriberComponent } from './subscriber/subscriber.component';
import { OpentokService } from './opentok.service';
import { createCustomElement } from '@angular/elements';
import { AngularDraggableModule } from 'angular2-draggable';
import { AppRoutingModule } from './routing/routing.component';
import { VideoPublisherComponent } from './pages/video-publisher/video-publisher.component';


@NgModule({
  declarations: [
    AppComponent,
    PublisherComponent,
    SubscriberComponent,
    VideoPublisherComponent
  ],
  imports: [
    BrowserModule,
    AngularDraggableModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    AppComponent,
    VideoPublisherComponent
  ]
})

export class AppModule {
  constructor(private injector: Injector) {
  }
  ngDoBootstrap() {
    const myCustomElement = createCustomElement(VideoPublisherComponent, { injector: this.injector });
      customElements.define('app-video-publisher', myCustomElement);
  }
}
