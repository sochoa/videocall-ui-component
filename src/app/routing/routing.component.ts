import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SubscriberComponent } from '../../app/subscriber/subscriber.component';
import { VideoPublisherComponent } from '../../app/pages/video-publisher/video-publisher.component';


const routes: Routes = [
  { path: 'publisher', component: VideoPublisherComponent },
  { path: 'join', component: SubscriberComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

