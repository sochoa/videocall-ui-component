import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { OpentokService } from '../../opentok.service';
import * as OT from '@opentok/client';
declare const require: any;

@Component({
  selector: 'app-video-publisher',
  templateUrl: './video-publisher.component.html',
  styleUrls: ['./video-publisher.component.css'],
  providers: [ OpentokService ]
})
export class VideoPublisherComponent implements OnInit {

  session: OT.Session;
  streams: Array<OT.Stream> = [];
  changeDetectorRef: ChangeDetectorRef;
  draggable = true;
  ngResizable = true;
  modalShare = false;
  token: string;

  openModal = {
    modalShare : false
  }

  constructor(
    private ref: ChangeDetectorRef,
    private opentokService: OpentokService
  ) {
    this.changeDetectorRef = ref;
  }

  ngOnInit () {
    this.opentokService.getToken(206).subscribe(token => this.token = token)
  }

  addSession(){
    this.opentokService.initSession(this.token).then((session: OT.Session) => {
      this.session = session;
      console.log(this.session, "entro sesion");
      this.session.on('streamCreated', (event) => {
        this.streams.push(event.stream);
        console.log(this.streams.length, "algosadddddddddddddddddddddddd");
        this.changeDetectorRef.detectChanges();
      });
      this.session.on('streamDestroyed', (event) => {
        const idx = this.streams.indexOf(event.stream);
        if (idx > -1) {
          this.streams.splice(idx, 1);
          this.changeDetectorRef.detectChanges();
        }
      });
    })
      .then(() => this.opentokService.connect())
      .catch((err) => {
        console.error(err);
        alert('Unable to connect. Make sure you have updated the config.ts file with your OpenTok details.');
      });
  }
}
