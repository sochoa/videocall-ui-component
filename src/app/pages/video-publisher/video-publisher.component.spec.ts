import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoPublisherComponent } from './video-publisher.component';

describe('VideoPublisherComponent', () => {
  let component: VideoPublisherComponent;
  let fixture: ComponentFixture<VideoPublisherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoPublisherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPublisherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
