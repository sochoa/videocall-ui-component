const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
    // NOTE: Have changed angular.json file, 'outputPath' to 'dist' rather than 'dist/<application-name>'. If you are using default angular.json then for file paths below, add <application-name> in file path. Example - './dist/my-medium/runtime.js', do the same for all.
    const files = [
        './dist/videocall-ui-component/runtime.js',
        './dist/videocall-ui-component/polyfills.js',
        './dist/videocall-ui-component/main.js',
        './dist/videocall-ui-component/scripts.js',
    ];
    await fs.ensureDir('dist');
    await concat(files, 'dist/videocall-ui-component/videocall-ui-component.js');
})();
